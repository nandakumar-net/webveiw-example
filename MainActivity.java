package com.example.webveiwer;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;

public class MainActivity extends Activity {
    Button b1;
    //This is used to add the object Button from the front_page.xml flie
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.front_page);
        //this line can veiw the .xml file so that we can use android codes in the MainActivity.java file
        b1= (Button) findViewById(R.id.b1);
        b1.setOnClickListener(new View.OnClickListener()//this line can execute the following lines the the object button is pressed {
            @Override
            public void onClick(View view) {
                Intent i1=new Intent(MainActivity.this,MyPage.class);
                startActivity(i1);
            }
        });
    }
}