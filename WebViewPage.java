package com.example.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class WebVeiwPage extends Activity {
    WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_me);
        mWebView = (WebView) findViewById(R.id.web);
        WebSettings webSettings = mWebView.getSettings();
        //this line imports the websettings for the objct Web View in page.xml
        webSettings.setJavaScriptEnabled(true);
        //this line enables the javascipt so that we can view the webpage.
        //If the line goes false you can't view the page.
        mWebView.loadUrl("http://beta.html5test.com/");
    }
} 